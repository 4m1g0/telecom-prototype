# Telecom-prototype

Se han incluido en el repositorio todas las dependencias externas, no es buena práctica, pero bueno,
es más cómodo, así no hay que instalar NPM, Bower, Grunt, etc. Si lo preferís, las quitamos.

Se ha hecho una limpieza del repositorio original, pero siguen estando disponibles TODOS los ejemplos
tal y como salen en la web de demo (puede ser útil para coger cosas rápidamente).

* Nuestro código va a ir, en un principio, en `/prototype`. Los `index.html` del raíz son los ejemplos tal
y como venían.

* Igual alguna vez tenemos que modificar algún estilo que venga por defecto. Con el fin de no tocar los CSS
originales, se ha incluido un `prototype/overwrite.css`.
# PYTHON SERVER
He añadido un archivo `simpleWebServer.py` que no es mas que (como su nombre indica) un servidor Web basado en python por si alguno no quiere instalar 
apache o nginx.
* Funcionamiento
He añadido un script `startServer.sh` que levanta el servidor web, la sintaxis es: `./startServer.sh [PORT]` es necesario indicar un numero de puerto, luego sencillamente es 
acceder a la pagina `http://localhost:[PORT]` y listo
