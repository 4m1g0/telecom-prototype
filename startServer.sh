#!/bin/bash
if [ $# -eq 0 ]
 then
	echo "Needs arguments for PORT"
 else	 
	echo "Starting Web Server on PORT $1"
	python -m SimpleHTTPServer $1
fi
